<?php

/**
 * @file
 *
 * Install hooks.
 */

/**
 * Implements hook_schema().
 */
function apachesolr_repeating_dates_schema() {
  $schema['apachesolr_repeating_dates'] = array(
    'description' => 'Index for repeating date fields.',
    'fields' => array(
      'field_id' => array(
        'description' => 'The primary identifier for a field.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'field_name' => array(
        'description' => 'The machine name of the field.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'field_human_name' => array(
        'description' => 'The human name of the field.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'solr_field_name' => array(
        'description' => 'The name of field indexed in solr.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'solr_single_field_name' => array(
        'description' => 'The name of the single start date field indexed in solr.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'solr_single_field_name_end' => array(
        'description' => 'The name of the single end date field indexed in solr.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
    'primary key' => array('field_id'),
  );
  return $schema;
}


/**
 * Implements hook_install().
 */
function apachesolr_repeating_dates_install() {
  // On install, make sure the index is up to date.
  $fields = apachesolr_repeating_dates_get_field_instances();

  foreach ($fields as $field) {
    apachesolr_repeating_dates_save_field($field['field_id'], $field['field_name'], $field['field_human_name']);
  }
}


/**
 * Implements hook_disable().
 */
function apachesolr_repeating_dates_disable() {
  drupal_set_message(t('Disabling/Uninstalling apachesolr_repeating_dates requires that all content in
 the Solr index is deleted and reindexed. See the README for more information'));
}
