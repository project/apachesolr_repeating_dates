
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Uninstalling

INTRODUCTION
------------

This module works to enhance the search experience when using
apachesolr and content types (nodes) with repeating date fields.

For each instance of a repeating date, the node is indexed in Solr as
a unique result. This means that if there is a node with a date that
repeats 3 times, that node will appear in the search results 3 times;
once for each date instance.


REQUIREMENTS
------------

This module requires the apachesolr module and as such also requires
an Apache Solr service to be setup. More information can be found in the
apachesolr module README.txt.

The date_repeat_field is also required for this module to work as expected


INSTALLATION
------------

Install as you would any Drupal module.

Once installed, reindex all content in Solr.


UNINSTALLING
------------

When disabling or uninstalling this module it can have a major impact
on search result behaviour.

Ensure that after disabling/uninstalling delete the Solr index and reindex.
