<?php

/**
 * @file
 * Database function calls.
 */

/**
 * Retrieve all fields stored in apachesolr_repeating_dates table.
 *
 * @return DatabaseStatementInterface|null
 *   A db_select result object.
 */
function apachesolr_repeating_dates_get_field_index() {
  $select = db_select('apachesolr_repeating_dates', 'ard')
    ->fields('ard')
    ->execute();
  return $select;
}


/**
 * Save or update a field in the module table.
 *
 * @param int $field_id
 *   ID of the field (not the instance ID).
 * @param string $field_name
 *   The field machine name.
 * @param string $field_human_name
 *   The field human name.
 */
function apachesolr_repeating_dates_save_field($field_id, $field_name, $field_human_name = '') {
  // Generate the solr single field names.
  list($date_start_name, $date_end_name) = _apachesolr_repeating_dates_prepare_field($field_name);
  // Generate the default solr field name.
  $solr_field_name = 'dm_' . $field_name;
  // Store the field.
  db_merge('apachesolr_repeating_dates')
    ->key(array('field_id' => $field_id))
    ->fields(array(
      'field_id' => $field_id,
      'field_name' => $field_name,
      'field_human_name' => $field_human_name,
      'solr_field_name' => $solr_field_name,
      'solr_single_field_name' => $date_start_name,
      'solr_single_field_name_end' => $date_end_name,
    ))
    ->execute();
}


/**
 * Delete a row from the module table.
 *
 * @param int $field_id
 *   The field ID (not the instance ID).
 */
function apachesolr_repeating_dates_delete_field($field_id) {
  // Delete field.
  db_delete('apachesolr_repeating_dates')
    ->condition('field_id', $field_id)
    ->execute();
}

/**
 * Retrieve the field ID from the field instance ID.
 *
 * @param int $instance_id
 *   The field instance ID.
 *
 * @return int
 *   The field ID.
 */
function apachesolr_repeating_dates_get_field_id_from_instance($instance_id) {
  $id = db_select('field_config_instance', 'fi')
    ->fields('fi', array('id'))
    ->condition('field_id', $instance_id, '=')
    ->execute()->fetchAssoc();
  return $id['id'];
}

/**
 * Retrieve the field human name from the instance ID.
 *
 * @param int $instance_id
 *   The field instance ID.
 *
 * @return string
 *   The field human name
 */
function apachesolr_repeating_dates_get_field_label_from_instance($instance_id) {
  $id = db_select('field_config_instance', 'fi')
    ->fields('fi', array('data'))
    ->condition('field_id', $instance_id, '=')
    ->execute()->fetchAssoc();
  $data = unserialize($id['data']);
  return $data['label'];
}


/**
 * Build out the Solr field names based on the given field machine name.
 *
 * @param string $field_name
 *   The field machine name.
 *
 * @return array
 *   An array containing the start and end solr field names.
 *   Use list($start, $end) to assign to variables.
 */
function _apachesolr_repeating_dates_prepare_field($field_name) {
  // Build field name: ds_ - date single.
  $date_start_name = 'ds_' . $field_name . '_single';
  $date_end_name = 'ds_' . $field_name . '_single_end';
  return array($date_start_name, $date_end_name);
}
